<%-- 
    Document   : ConfirmChange
    Created on : Jul 1, 2018, 2:17:47 PM
    Author     : PhuNDSE63159
--%>

<%@page import="java.util.List"%>
<%@page import="phund.tbldepartment.tblDepartmentDTO"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Confirm</title>
    </head>
    <body>
        <%--<c:set var="departmentDTO" value="${sessionScope.DEPARTMENT}"/>--%>
        <c:set var="listDepartment" value="${requestScope.DEPARTMENTLIST}"/>
        <c:set var="employeeDTO"  value="${requestScope.EMPLOYEEDTO}"/>

        <h3>Department Id of employee: ${employeeDTO.depId}</h3>
        <br>
        <form action="changeDepartmentSalary">
            To department 
            <select name="departmentChoice">
                <c:forEach var="dto" items="${listDepartment}">
                    <option>
                        ${dto.depId} - ${dto.name}
                    </option>
                </c:forEach>
            </select>
            <br>
            Salary <input type="text" name="salary" value="${employeeDTO.salary}" />
            <input type="hidden" name="employeeId" value="${employeeDTO.employeeId}" />
            <br>
            <input type="submit" value="Confirm" name="btnSubmit"/> 
            <input type="submit" value="Cancel" name="btnSubmit"/>
            <input type="hidden" name="txtFrom" value="${param.txtFrom}"/>
            <input type="hidden" name="txtTo" value="${param.txtTo}" />
        </form>
        <c:set var="error" value="${requestScope.ERROR}"/>
        <c:if test="${not empty error}">
            <font color="red">
                ${error.salaryFormatError}
            </font>
        </c:if>
    </body>
</html>
