<%-- 
    Document   : searchPageStaff
    Created on : Jun 29, 2018, 4:23:36 PM
    Author     : PhuNDSE63159
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Search Page For Staff</title>
    </head>
    <body>
        <c:set var="userObj" value="${sessionScope.EMPLOYEE}" />
        <a href="logout">Logout</a>
        <h1>
            <font color="red">
            Welcome, ${userObj.employeeId}
            </font>
        </h1>
        EmployeeId: ${userObj.employeeId} <br>
        Name: ${userObj.name} <br>
        Department: ${userObj.depId} <br>
        Salary: ${userObj.salary} <br>
        Leave Date:<br>
        <form action="addRequest" method="post">
            From (yyyy-mm-dd) 
            <input type="text" name="txtFrom" value="${param.txtFrom}" />
            <br>
            To (yyyy-mm-dd) 
            <input type="text" name="txtTo" value="${param.txtTo}" />
            <br>
            Reason 
            <textarea name="txtReason"></textarea>
            <br>
            <input type="submit" value="Request" />
        </form>
        <c:set var="errors" value="${requestScope.ERRORS}"/>
        <c:if test="${not empty errors}">
            <font color="red">
            ${errors.dateFormatError}
            </font>
        </c:if>
        <%--<c:if test="${empty errors}">
            <font color="red">
            Successfully!!! Request is waiting for proccessing
            </font>
        </c:if> --%>
        <c:import url="viewAllLeaves.jsp"/>
    </body>
</html>
