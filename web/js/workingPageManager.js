function confirmAccept() {
	swal({
	  	title: "Are you sure to accept?",
	  	icon: "warning",
	 	buttons: {
	  		cancel: true,
	   		confirm: true,
	   	},
	})
	.then((willChange) => {
	  if (willChange) {
	  	console.log("true");
	    return true;
	  } 
	});
	return false;
}

function confirmReject() {
	swal({
	  title: "Are you sure to reject?",
	  icon: "warning",
	  buttons: true
	})
	.then((willChange) => {
	  if (willChange) {
	  	console.log("true");
	    return true;
	  } 
	});
	return false;
}