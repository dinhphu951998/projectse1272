<%-- 
    Document   : search
    Created on : Jun 29, 2018, 3:19:22 PM
    Author     : PhuNDSE63159
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Search Page For Manager</title>
    </head>
    <body>
        <c:set var="dtoEmp" value="${sessionScope.EMPLOYEE}"/>
        <c:set var="dtoDep" value="${sessionScope.DEPARTMENT}"/>
        <a href="logout">Logout</a>
        <h2>Department Name: <font color="red"> ${dtoDep.name}</font> </h2>
        <h2>Total Employee: <font color="red"> ${sessionScope.TOTALEMPLOYEE}</font> </h2><br>
        <form action="search" method="post">
            From (yyyy-mm-dd) <input type="text" name="txtFrom" value="${param.txtFrom}" /> <br>
            <br>
            To (yyyy-mm-dd) <input type="text" name="txtTo" value="${param.txtTo}" /> <br>
            <br>
            <input type="submit" value="Search" />
        </form>
        <c:if test="${not empty param.txtFrom and not empty param.txtTo}">
            <c:set var="errors" value="${requestScope.ERRORS}"/>
            <c:if test="${not empty errors}">
                <font color="">
                <h1>
                    Date format must be yyyy-mm-dd
                </h1>
                </font>
            </c:if>
            <c:if test="${empty errors}">
                <c:set var="listLeave" value="${requestScope.LIST}"/>
                <c:if test="${not empty listLeave}">
                    <table border="1">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>EmployeeID</th>
                                <th>Name</th>
                                <th>Salary</th>
                                <th>Address</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Request Reason</th>
                                <th>Reject Reason</th>
                                <th>Update</th>
                                <th>Action</th>
                            </tr>   
                        </thead>
                        <tbody>
                            <c:forEach var="dto" items="${listLeave}" varStatus="counter">
                            <form action="update" method="post">
                                <tr>
                                    <td>
                                        ${counter.count}
                                    </td>
                                    <td>
                                        ${dto.employeeId}
                                    </td>
                                    <td>
                                        ${dto.name}
                                    </td>
                                    <td>
                                        ${dto.salary}
                                    </td>
                                    <td>
                                        ${dto.address}
                                    </td>
                                    <td>
                                        ${dto.email}
                                    </td>
                                    <td>
                                        ${dto.phone}
                                    </td>
                                    <td>
                                        ${dto.requestReason}

                                    </td>
                                    <td>
                                        <textarea name="txtRejectReason" rows="4" cols="20">${dto.rejectReason}</textarea>
                                    </td>
                                    <td>
                                        <input type="submit" value="Accept" name="btnSubmit" onclick="return confirmRequest('accept')" /><br>
                                        <input type="submit" value="Reject" name="btnSubmit" onclick="return confirmRequest('reject')"/>
                                        <input type="hidden" name="txtLeaveId" value="${dto.leaveId}" />
                                        <input type="hidden" name="txtFrom" value="${param.txtFrom}" />
                                        <input type="hidden" name="txtTo" value="${param.txtTo}" />
                                    </td>
                                    <td>
                                        <c:url var="changeLink" value="change">
                                            <c:param name="employeeId" value="${dto.employeeId}"/>
                                            <c:param name="txtFrom" value="${param.txtFrom}"/>
                                            <c:param name="txtTo" value="${param.txtTo}"/>
                                        </c:url>
                                        <a href="${changeLink}">Change</a>
                                    </td>
                                </tr>
                            </form>
                        </c:forEach>
                    </tbody>
                </table>
            </c:if>
            <c:if test="${empty listLeave}">
                <h2>
                    No record
                </h2>
            </c:if>
        </c:if>
    </c:if>
</body>
<script>
    function confirmRequest(argument) {
        var result = confirm("Are you sure to " + argument + " this?");
        if (result === true) {
            return true;
        }
        return false;
    }
</script>
</html>
