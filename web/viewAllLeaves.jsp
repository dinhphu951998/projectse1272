<%-- 
    Document   : viewAllLeaves
    Created on : Jul 1, 2018, 8:35:48 PM
    Author     : PhuNDSE63159
--%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:set var="list" value="${requestScope.ALLLEAVES}"/>
        <c:if test="${not empty list}">
            <h4>View all leaves</h4>
            <table border="1">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>from date</th>
                        <th>to date</th>
                        <th>Request Reason</th>
                        <th>Accept</th>
                        <th>Reject reason</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="leave" items="${list}" varStatus="counter">
                        <tr>
                            <td>
                                ${counter.count}
                            </td>
                            <td>
                                <%-- <fmt:formatDate var="fromDateFormat" value="${leave.fromDate}"/>
                                ${fromDateFormat} --%>
                                ${leave.fromDate}
                            </td>
                            <td>
                                <%--<fmt:formatDate var="toDateFormat" value="${leave.toDate}"/>
                                ${toDateFormat} --%>
                                ${leave.toDate}
                            </td>
                            <td>
                                ${leave.requestReason}
                            </td>
                            <td>
                                <c:if test="${leave.accept}">
                                    <font color="green">Accept</font>
                                </c:if>
                                <c:if test="${not leave.accept and leave.rejectReason != 'Pending'}">
                                    <font color="red">Reject</font>
                                </c:if>
                                <c:if test="${not leave.accept and leave.rejectReason == 'Pending'}">
                                    Waiting
                                </c:if>
                            </td>
                            <td>
                                ${leave.rejectReason}
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </c:if>
    </body>
</html>
