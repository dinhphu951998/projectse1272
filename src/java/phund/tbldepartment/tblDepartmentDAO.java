/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phund.tbldepartment;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import phund.utils.DBConnection;

/**
 *
 * @author PhuNDSE63159
 */
public class tblDepartmentDAO implements Serializable {

    private Connection c;
    private PreparedStatement psm;
    private ResultSet rs;
    private List<tblDepartmentDTO> listDepartment;

    public List<tblDepartmentDTO> getListDepartment() {
        return listDepartment;
    }
    
    private void closeConnection() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (psm != null) {
            psm.close();
        }
        if (c != null) {
            c.close();
        }
    }

    public tblDepartmentDTO getDepartmentById(String depId) throws NamingException, SQLException {
        tblDepartmentDTO dto = null;
        try {
            c = DBConnection.makeConnection();
            String sql = "select name from tbl_department where depID = ?";
            psm = c.prepareStatement(sql);
            psm.setString(1, depId);
            rs = psm.executeQuery();
            if (rs.next()) {
                String name = rs.getString("name");
                dto = new tblDepartmentDTO(depId, name);
            }
        } finally {
            closeConnection();
        }
        return dto;
    }

    public void getAllDepartment() throws NamingException, SQLException {
        try {
            c = DBConnection.makeConnection();
            String sql = "select depId, name from tbl_department";
            psm = c.prepareStatement(sql);
            rs = psm.executeQuery();
            while (rs.next()) {
                String depId = rs.getString("depId");
                String name = rs.getString("name");
                if(listDepartment == null){
                    listDepartment = new ArrayList<>();
                }
                listDepartment.add(new tblDepartmentDTO(depId, name));
            }
        } finally {
            closeConnection();
        }
    }
}
