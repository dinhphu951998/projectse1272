/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phund.tbldepartment;

import java.io.Serializable;

/**
 *
 * @author PhuNDSE63159
 */
public class tblDepartmentDTO implements Serializable {

    private String depId;
    private String name;

    public tblDepartmentDTO() {
    }

    public tblDepartmentDTO(String depId, String name) {
        this.depId = depId;
        this.name = name;
    }

    /**
     * @return the depId
     */
    public String getDepId() {
        return depId;
    }

    /**
     * @param depId the depId to set
     */
    public void setDepId(String depId) {
        this.depId = depId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

}
