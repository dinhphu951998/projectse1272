/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phund.tblemployee;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.naming.NamingException;
import phund.utils.DBConnection;

/**
 *
 * @author PhuNDSE63159
 */
public class tblEmployeeDAO implements Serializable {

    private Connection c;
    private PreparedStatement psm;
    private ResultSet rs;

    private void closeConnection() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (psm != null) {
            psm.close();
        }
        if (c != null) {
            c.close();
        }
    }

    public tblEmployeeDTO getEmployee(String employeeId) throws NamingException, SQLException {
        tblEmployeeDTO dto = null;
        try {
            c = DBConnection.makeConnection();
            String sql = "select  name, address, email, phone, manager, depID, salary "
                    + "from tbl_employee "
                    + "where employeeID = ?";
            psm = c.prepareStatement(sql);
            psm.setString(1, employeeId);
            rs = psm.executeQuery();
            if (rs.next()) {
                String name = rs.getString("name");
                String address = rs.getString("address");
                String email = rs.getString("email");
                String phone = rs.getString("phone");
                boolean isManager = rs.getBoolean("manager");
                String depId = rs.getString("depID");
                float salary = rs.getFloat("salary");
                dto = new tblEmployeeDTO(employeeId, name, address, email, phone, isManager, depId, salary);
                return dto;
            }
        } finally {
            closeConnection();
        }
        return dto;
    }

    public int getTotalEmployee(String depId) throws SQLException, NamingException {
        int total = -1;
        try {
            c = DBConnection.makeConnection();
            String sql = "select count(employeeId) as totalEmployee "
                    + "from tbl_employee "
                    + "where depId = ? "
                    + "group by depId";
            psm = c.prepareStatement(sql);
            psm.setString(1, depId);
            rs = psm.executeQuery();
            if (rs.next()) {
                total = rs.getInt("totalEmployee");
            }
        } finally {
            closeConnection();
        }
        return total;
    }

    public boolean updateEmployee(String empId, String depId, float salary) throws NamingException, SQLException {
        int total = -1;
        try {
            c = DBConnection.makeConnection();
            String sql = "update tbl_employee set depId = ?, salary = ? where employeeID = ?";
            psm = c.prepareStatement(sql);
            psm.setString(1, depId);
            psm.setFloat(2, salary);
            psm.setString(3, empId);
            total = psm.executeUpdate();
            if (total > 0) {
                return true;
            }
        } finally {
            closeConnection();
        }
        return false;
    }
}
