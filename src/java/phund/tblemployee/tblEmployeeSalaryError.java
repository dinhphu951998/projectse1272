/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phund.tblemployee;

import java.io.Serializable;

/**
 *
 * @author PhuNDSE63159
 */
public class tblEmployeeSalaryError implements Serializable {

    private String salaryFormatError;

    public tblEmployeeSalaryError() {
    }

    public void setSalaryFormatError(String salaryFormatError) {
        this.salaryFormatError = salaryFormatError;
    }

    public String getSalaryFormatError() {
        return salaryFormatError;
    }

}
