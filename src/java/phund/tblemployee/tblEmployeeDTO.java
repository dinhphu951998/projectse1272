/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phund.tblemployee;

import java.io.Serializable;

/**
 *
 * @author PhuNDSE63159
 */
public class tblEmployeeDTO implements Serializable {

    private String employeeId;
    private String name;
    private String address;
    private String email;
    private String phone;
    private boolean isManager;
    private String depId;
    private float salary;

    public tblEmployeeDTO() {
    }

    public tblEmployeeDTO(String employeeId, String name, String address, String email, String phone, boolean isManager, String depId, float salary) {
        this.employeeId = employeeId;
        this.name = name;
        this.address = address;
        this.email = email;
        this.phone = phone;
        this.isManager = isManager;
        this.depId = depId;
        this.salary = salary;
    }

    /**
     * @return the employeeId
     */
    public String getEmployeeId() {
        return employeeId;
    }

    /**
     * @param employeeId the employeeId to set
     */
    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the isManager
     */
    public boolean isIsManager() {
        return isManager;
    }

    /**
     * @param isManager the isManager to set
     */
    public void setIsManager(boolean isManager) {
        this.isManager = isManager;
    }

    /**
     * @return the depId
     */
    public String getDepId() {
        return depId;
    }

    /**
     * @param depId the depId to set
     */
    public void setDepId(String depId) {
        this.depId = depId;
    }

    /**
     * @return the salary
     */
    public float getSalary() {
        return salary;

    }

    /**
     * @param salary the salary to set
     */
    public void setSalary(float salary) {
        this.salary = salary;
    }

}
