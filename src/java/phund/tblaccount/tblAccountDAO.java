/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phund.tblaccount;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.naming.NamingException;
import phund.utils.DBConnection;

/**
 *
 * @author PhuNDSE63159
 */
public class tblAccountDAO implements Serializable {

    private Connection c;
    private PreparedStatement psm;
    private ResultSet rs;

    private void closeConnection() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (psm != null) {
            psm.close();
        }
        if (c != null) {
            c.close();
        }
    }

    public int checkLogin(String accountId, String password) throws SQLException, NamingException {
        int role = -1;
        try {
            c = DBConnection.makeConnection();
            String sql = "select role from tbl_account where accountId = ? and password = ?";
            psm = c.prepareStatement(sql);
            psm.setString(1, accountId);
            psm.setString(2, password);
            rs = psm.executeQuery();
            if (rs.next()) {
                role = rs.getInt("role");
            }
        } finally {
            closeConnection();
        }
        return role;
    }

}
