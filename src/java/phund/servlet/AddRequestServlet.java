/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phund.servlet;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phund.tblemployee.tblEmployeeDTO;
import phund.tblleave.tblLeaveDAO;
import phund.tblleave.tblLeaveError;

/**
 *
 * @author PhuNDSE63159
 */
@WebServlet(name = "AddRequestServlet", urlPatterns = {"/AddRequestServlet"})
public class AddRequestServlet extends HttpServlet {

    private final String workingPageEmployee = "workingPageEmployee.jsp";
    private final String viewAllLeavesServlet = "viewAllLeaves";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String txtFrom = request.getParameter("txtFrom");
        String txtTo = request.getParameter("txtTo");
        String txtReason = request.getParameter("txtReason").trim();
        tblLeaveError errors = null;
        String url = workingPageEmployee;
        try {
            HttpSession session = request.getSession(false);
            if (session != null) {
                tblEmployeeDTO employee = (tblEmployeeDTO) session.getAttribute("EMPLOYEE");
                Date fromDate = Date.valueOf(txtFrom);
                Date toDate = Date.valueOf(txtTo);
                String empId = employee.getEmployeeId();
                tblLeaveDAO dao = new tblLeaveDAO();
                boolean res = dao.addRequest(fromDate, toDate, txtReason, empId);
                if (res) {
                    url = viewAllLeavesServlet;
                }
            }
        } catch (IllegalArgumentException ex) {
            log("RequestServlet_IllegalArgumentException " + ex.getMessage());
            errors = new tblLeaveError();
            errors.setDateFormatError("Date format must be yyyy-mm-dd");
            request.setAttribute("ERRORS", errors);
        } catch (NamingException ex) {
            Logger.getLogger(AddRequestServlet.class.getName()).log(Level.SEVERE, null, ex);
            log("RequestServlet_NamingException " + ex.getMessage());
        } catch (SQLException ex) {
            Logger.getLogger(AddRequestServlet.class.getName()).log(Level.SEVERE, null, ex);
            log("RequestServlet_SQLException " + ex.getMessage());
        } finally {
            if (errors != null) { // có lỗi => request dispatcher
                RequestDispatcher rd = request.getRequestDispatcher(url);
                rd.forward(request, response);
            } else {
                response.sendRedirect(url);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
