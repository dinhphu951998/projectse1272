/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phund.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phund.tbldepartment.tblDepartmentDTO;
import phund.tblemployee.tblEmployeeDAO;
import phund.tblemployee.tblEmployeeSalaryError;

/**
 *
 * @author PhuNDSE63159
 */
@WebServlet(name = "ConfirmServlet", urlPatterns = {"/ConfirmServlet"})
public class ConfirmServlet extends HttpServlet {

    private final String searchServlet = "search";
    private final String errorPage = "errorPage.html";
    private final String startUpServlet = "startUp";
    private final String changeServlet = "ChangeServlet";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = errorPage;
        String departmentChoice = request.getParameter("departmentChoice");
        String salaryString = request.getParameter("salary");
        String employeeId = request.getParameter("employeeId");
        String txtFrom = request.getParameter("txtFrom");
        String txtTo = request.getParameter("txtTo");
        HttpSession session = request.getSession(false);
        tblEmployeeSalaryError error = null;
        try {
            if (session != null) {
                float salaryFloat = Float.parseFloat(salaryString);
                String departmentIdChangeTo = departmentChoice.split("-")[0];
                tblEmployeeDAO employeeDAO = new tblEmployeeDAO();
                boolean res = employeeDAO.updateEmployee(employeeId, departmentIdChangeTo, salaryFloat);
                if (res) {
                    url = searchServlet
                            + "?txtFrom=" + txtFrom
                            + "&txtTo=" + txtTo;
                    tblDepartmentDTO sessionDepartmentDTO = (tblDepartmentDTO) session.getAttribute("DEPARTMENT");
                    int total = employeeDAO.getTotalEmployee(sessionDepartmentDTO.getDepId());
                    session.setAttribute("TOTALEMPLOYEE", total);
                }
            } else {
                url = startUpServlet;
            }
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
            error = new tblEmployeeSalaryError();
            error.setSalaryFormatError("Salary format is not expected. It must be a float");
            request.setAttribute("ERROR", error);
            url = changeServlet;
            log("ConfirmServlet_NumberFormatException " + ex.getMessage());
        } catch (NamingException ex) {
            Logger.getLogger(ConfirmServlet.class.getName()).log(Level.SEVERE, null, ex);
            log("ConfirmServlet_NamingException " + ex.getMessage());
        } catch (SQLException ex) {
            Logger.getLogger(ConfirmServlet.class.getName()).log(Level.SEVERE, null, ex);
            log("ConfirmServlets_SQLException " + ex.getMessage());
        } finally {
            if (error != null) {
                RequestDispatcher rd = request.getRequestDispatcher(url);
                rd.forward(request, response);
            } else {
                response.sendRedirect(url);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
