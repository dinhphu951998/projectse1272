/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phund.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phund.tblaccount.tblAccountDAO;
import phund.tblemployee.tblEmployeeDAO;
import phund.tblemployee.tblEmployeeDTO;

/**
 *
 * @author PhuNDSE63159
 */
@WebServlet(name = "StartUpServlet", urlPatterns = {"/StartUpServlet"})
public class StartUpServlet extends HttpServlet {

    private final String loginPage = "login.html";
    private final String loginServlet = "login";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = loginPage;
        int role = -1;
        try {
            Cookie[] cookies = request.getCookies();
            if (cookies != null) {
                String username = null;
                for (int i = 0; i < cookies.length; i++) {
                    String accountId = cookies[i].getName();
                    String password = cookies[i].getValue();
                    tblAccountDAO dao = new tblAccountDAO();
                    role = dao.checkLogin(accountId, password);
                    if (role != -1) {
                        username = accountId;
//                        if (role == 1) {
//                            url = workingPageManager;
//                        } else if (role == 2) {
//                            url = viewAllLeavesServlet;
//                        }
                        url = loginServlet
                                + "?username="
                                + username
                                + "&password="
                                + password;
                    }
                }
//                if (username != null) {
//                    tblEmployeeDAO dao = new tblEmployeeDAO();
//                    tblEmployeeDTO dto = dao.getEmployee(username);
//                    HttpSession session = request.getSession();
//                    session.setAttribute("EMPLOYEE", dto);
//                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(StartUpServlet.class.getName()).log(Level.SEVERE, null, ex);
            log("StartUpServlet_SQLException " + ex.getMessage());
        } catch (NamingException ex) {
            Logger.getLogger(StartUpServlet.class.getName()).log(Level.SEVERE, null, ex);
            log("StartUpServlet_NamingException " + ex.getMessage());
        } finally {
            response.sendRedirect(url);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
