/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phund.tblleave;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author PhuNDSE63159
 */
public class tblLeaveDTO implements Serializable {

    private int leaveId;
    private Date fromDate;
    private Date toDate;
    private boolean accept;
    private String empId;
    private String requestReason;
    private String rejectReason;

    public tblLeaveDTO() {
    }

    public tblLeaveDTO(int leaveId, Date fromDate, Date toDate, boolean accept, String empId, String requestReason, String rejectReason) {
        this.leaveId = leaveId;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.accept = accept;
        this.empId = empId;
        this.requestReason = requestReason;
        this.rejectReason = rejectReason;
    }
    
    

    /**
     * @return the leaveId
     */
    public int getLeaveId() {
        return leaveId;
    }

    /**
     * @param leaveId the leaveId to set
     */
    public void setLeaveId(int leaveId) {
        this.leaveId = leaveId;
    }

    /**
     * @return the fromDate
     */
    public Date getFromDate() {
        return fromDate;
    }

    /**
     * @param fromDate the fromDate to set
     */
    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    /**
     * @return the toDate
     */
    public Date getToDate() {
        return toDate;
    }

    /**
     * @param toDate the toDate to set
     */
    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    /**
     * @return the accept
     */
    public boolean isAccept() {
        return accept;
    }

    /**
     * @param accept the accept to set
     */
    public void setAccept(boolean accept) {
        this.accept = accept;
    }

    /**
     * @return the empId
     */
    public String getEmpId() {
        return empId;
    }

    /**
     * @param empId the empId to set
     */
    public void setEmpId(String empId) {
        this.empId = empId;
    }

    /**
     * @return the requestReason
     */
    public String getRequestReason() {
        return requestReason;
    }

    /**
     * @param requestReason the requestReason to set
     */
    public void setRequestReason(String requestReason) {
        this.requestReason = requestReason;
    }

    /**
     * @return the rejectReason
     */
    public String getRejectReason() {
        return rejectReason;
    }

    /**
     * @param rejectReason the rejectReason to set
     */
    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

}
