/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phund.tblleave;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import phund.utils.DBConnection;

/**
 *
 * @author PhuNDSE63159
 */
public class tblLeaveDAO implements Serializable {

    private Connection c;
    private PreparedStatement psm;
    private ResultSet rs;
    private List<tblLeaveDTO> listLeave;
    private List<tblEmployeeLeaveDTO> listEmpLeave;

    public List<tblLeaveDTO> getListLeave() {
        return listLeave;
    }

    private void closeConnection() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (psm != null) {
            psm.close();
        }
        if (c != null) {
            c.close();
        }
    }

    public boolean addRequest(Date fromDate, Date toDate, String reason, String empId) throws NamingException, SQLException {
        int res = 0;
        try {
            c = DBConnection.makeConnection();
            String sql = "insert into tbl_leave(fromDate, toDate, requestReason, empID, rejectReason) "
                    + "values(?,?,?,?,?)";
            psm = c.prepareStatement(sql);
            psm.setDate(1, fromDate);
            psm.setDate(2, toDate);
            psm.setString(3, reason);
            psm.setString(4, empId);
            psm.setString(5, "Pending");
            res = psm.executeUpdate();
            if (res > 0) {
                return true;
            }
        } finally {
            closeConnection();
        }
        return false;
    }

//    public void getRequestByFromTo(Date from, Date to) throws NamingException, SQLException {
//        try {
//            c = DBConnection.makeConnection();
//            String sql = "select leaveID, fromDate, toDate, accept, empID, requestReason, rejectReason "
//                    + "from tbl_leave "
//                    + "wherer fromDate < ? and toDate > ?";
//            psm = c.prepareStatement(sql);
//            psm.setDate(1, from);
//            psm.setDate(2, to);
//            while (rs.next()) {
//                int id = rs.getInt("leaveID");
//                Date fromDate = rs.getDate("fromDate");
//                Date toDate = rs.getDate("toDate");
//                boolean accept = rs.getBoolean("accept");
//                String empID = rs.getString("empID");
//                String requestReason = rs.getString("requestReason");
//                String rejectReason = rs.getString("rejectReason");
//                tblLeaveDTO dto = new tblLeaveDTO(id, fromDate, toDate, accept, empID, requestReason, rejectReason);
//                if (listLeave == null) {
//                    listLeave = new ArrayList<tblLeaveDTO>();
//                }
//                listLeave.add(dto);
//            }
//        } finally {
//            closeConnection();
//        }
//    }
    public void getAllLeavesByEmpId(String empID) throws NamingException, SQLException {
        try {
            c = DBConnection.makeConnection();
            String sql = "select leaveID, fromDate, toDate, accept, requestReason, rejectReason "
                    + "from tbl_leave "
                    + "where empID = ? ";
            psm = c.prepareStatement(sql);
            psm.setString(1, empID);
            rs = psm.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("leaveID");
                Date fromDate = rs.getDate("fromDate");
                Date toDate = rs.getDate("toDate");
                boolean accept = rs.getBoolean("accept");
                String requestReason = rs.getString("requestReason");
                String rejectReason = rs.getString("rejectReason");
                tblLeaveDTO dto = new tblLeaveDTO(id, fromDate, toDate, accept, empID, requestReason, rejectReason);
                if (listLeave == null) {
                    listLeave = new ArrayList<tblLeaveDTO>();
                }
                listLeave.add(dto);
            }
        } finally {
            closeConnection();
        }
    }

    public void getRequestByFromTo(Date from, Date to) throws NamingException, SQLException {
        try {
            c = DBConnection.makeConnection();
            String sql = "select l.leaveID, e.employeeId, e.name, e.address, e.email, e.phone, e.salary, l.requestReason, l.rejectReason  "
                    + "from tbl_leave l join tbl_employee e on l.empID = e.employeeID  "
                    + "where l.fromDate >= ? and l.toDate <= ? ";
            psm = c.prepareStatement(sql);
            psm.setDate(1, from);
            psm.setDate(2, to);
            rs = psm.executeQuery();
            while (rs.next()) {
                int leaveId = rs.getInt("leaveID");
                String empID = rs.getString("employeeId");
                String name = rs.getString("name");
                String address = rs.getString("address");
                String email = rs.getString("email");
                String phone = rs.getString("phone");
                float salary = rs.getFloat("salary");
                String requestReason = rs.getString("requestReason");
                String rejectReason = rs.getString("rejectReason");
                if (listEmpLeave == null) {
                    listEmpLeave = new ArrayList<>();
                }
                listEmpLeave.add(new tblEmployeeLeaveDTO(leaveId, empID, name, address, email, phone, salary, requestReason, rejectReason));
            }
        } finally {
            closeConnection();
        }
    }

    public List<tblEmployeeLeaveDTO> getListEmpLeave() {
        return listEmpLeave;
    }

    public boolean updateRecord(int leaveId, boolean accept, String rejectReason) throws NamingException, SQLException {
        int res = -1;
        try {
            c = DBConnection.makeConnection();
            String sql = "update tbl_leave set accept = ?, rejectReason = ? where leaveID = ? ";
            psm = c.prepareStatement(sql);
            psm.setBoolean(1, accept);
            psm.setString(2, rejectReason);
            psm.setInt(3, leaveId);
            res = psm.executeUpdate();
            if (res > 0) {
                return true;
            }
        } finally {
            closeConnection();
        }
        return false;
    }
    

}
